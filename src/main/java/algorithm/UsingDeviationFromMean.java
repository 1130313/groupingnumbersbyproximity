package algorithm;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

import algorithm.exception.InsufficientNumbersException;

public class UsingDeviationFromMean implements GroupNumbersByProximityAlgorithm{
	/**
	 * M�todo que divide um array de Point em partes
	 * @param array array a dividir
	 * @param nrParts numero de partes
	 * @return array dividido em partes iguais
	 * caso a divis�o de quantidade de Point / nrParts, o resto � repartido por as partes iniciais
	 * Ex: array de 16 Point / 3 parts = grupo com 6 grupo com 5 grupo com 5 
	 */
	public List<List<Point>> split(List<Point> array, int nrParts){
		int step = array.size() / nrParts;
		 int remaining = array.size() % nrParts;
		
		 List<List<Point>> parts = new ArrayList<>();
		 
		 int index = 0;
		 
		 for(int i = 0; i < nrParts ;i++)
		 {
			 List<Point> sublist;
			 if(remaining!=0)
			 {
				 sublist = new ArrayList<>(array.subList(index, index + step + 1));
				 remaining--;
				 index += step + 1;
			 }
			 else
			 {
				 sublist = new ArrayList<>(array.subList(index,index + step));
				 index+=step;
			 }
			 for(Point p : sublist) p.setGroupIndex(i);
			 parts.add(sublist);
		 }
		 return parts;
	}
	/**
	 * Calculo da m�dia
	 * @param group grupo a calcular
	 * @return soma de todos os valores presentes no grupo a dividir pelo total
	 */
	public Double calculateMean(List<Point> group)
	{
		Integer sum = group.stream().map(x->x.getNumber()).reduce(0,(x,y)->x+y);
		Integer count = group.size();
		if(count>0)
			return (double) sum / count;
		else
			return 0.0;
	}
	// formula de calculo de novo centro, media de cada grupo
	/**
	 * M�todo que calcula a m�dia de cada grupo 
	 * @param groups grupos
	 * @param means array que guarda as medias
	 */
	public void calculateMeans(List<List<Point>> groups,Double [] means)
	{
		for(int i = 0;i< groups.size();i++)
		{
			List<Point> group = groups.get(i);
			means[i] = calculateMean(group);
		}
	}
	/**
	 * Algoritmo que dividi os pontos em grupos conforme a proximidade
	 * @param points grupo de pontos
	 * @param nrGroups quantidade de grupos
	 * @return List<List<Point>> grupos agrupados por proximidade
	 */
	public List<List<Point>> algorithmUsingDeviationFromMean(List<Point> points, int nrGroups)
	{
		//dividir o array em partes iguais conforme o n�mero de grupos
		List<List<Point>> groups = split(points, nrGroups);
		
		//criacao de uma matrix auxiliar para transferir os pontos.
		List<List<Point>> groups_aux = new ArrayList<>();
		for(int i = 0;i<nrGroups;i++)
		{
			ArrayList<Point> group_aux = new ArrayList<>();
			groups_aux.add(group_aux);
		}
		//calculo dos medias iniciais de cada grupo criado
		Double [] means = new Double[nrGroups];
		calculateMeans(groups,means);
		
		//variavel que guarda as distancias de cada ponto a cada m�dia
		Double [] distances = new Double[nrGroups];
		
		boolean change = true;
		//Enquanto existir pelo menos uma permuta de pontos
		while(change)
		{
			change = false;
			for(Point p : points)
			{
				double min = 1000000000;
				int index = 0;
				for(int j=0;j<distances.length;j++)
				{
					distances[j] = Point.distanceBetweenPointAndCenter(p,means[j]);
					if(distances[j]<= min)
					{
						min = distances[j];
						index = j;
					}
				}
				if(index != p.getGroupIndex()) change = true; 
				p.setGroupIndex(index);
				groups_aux.get(index).add(p);
			}
			//move dos pontos do grupo auxiliar para o grupo final
			groups.forEach(x->x.clear());
			for(int j = 0 ; j < groups.size(); j++)
			{
				groups.get(j).addAll(groups_aux.get(j));
			}
			groups_aux.forEach(x->x.clear());	
			calculateMeans(groups, means);
		}
		return groups;
	}
	/**
	 * Metodo que calcula a distancia entre um ponto e a m�dia
	 * @param point Ponto
	 * @param mean M�dia
	 * @return distancia
	 */
	public Double distanceBetweenPointAndMean(Integer point, Double mean)
	{
		return Math.abs(point-mean);
	}
	
	@Override
	public Integer[][] group(Integer[] array, int nrGroups) throws InsufficientNumbersException {
		// TODO Auto-generated method stub
		
		//Restri��o caso o nr de grupos > tamanho do array
		if(nrGroups>array.length) throw new InsufficientNumbersException("The groups must are less or equal to the total number of Integers");
		
		//convers�o do array em lista
		List<Integer> arrayAsList = Arrays.asList(array);
		
		//ordenar o array por ordem crescente
		Collections.sort(arrayAsList);

		//converter o array de Integer para array de Point
		List<Point> points = arrayAsList.stream().map(Point::new).collect(Collectors.toCollection(ArrayList::new));
		//Usar o algoritmo
		List<List<Point>> groups = algorithmUsingDeviationFromMean(points,nrGroups);
		
		//conversao de matriz de Point para matriz de Integer
		Integer [][] result = new Integer[nrGroups][];
		for(int i = 0;i<result.length;i++)
		{
			List<Integer> gp = groups.get(i).stream().map(x->x.getNumber()).collect(Collectors.toList());
			result[i] = new Integer [gp.size()];
			result[i] = gp.toArray(result[i]);
		}
		return result;
	}

	
}
