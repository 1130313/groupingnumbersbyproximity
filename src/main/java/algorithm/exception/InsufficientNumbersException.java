package algorithm.exception;


public class InsufficientNumbersException extends Exception {
	  public InsufficientNumbersException() { super(); }
	  public InsufficientNumbersException(String message) { super(message); }
	  public InsufficientNumbersException(String message, Throwable cause) { super(message, cause); }
	  public InsufficientNumbersException(Throwable cause) { super(cause); }
}
