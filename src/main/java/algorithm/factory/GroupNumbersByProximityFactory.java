package algorithm.factory;

import java.util.logging.Level;
import java.util.logging.Logger;

import algorithm.GroupNumbersByProximityAlgorithm;
import settings.Settings;

public class GroupNumbersByProximityFactory {

	private final String PACKAGE_PREFIX = "algorithm";
    /**
     * Returns a new instance of TransformationFactory
     * @return TransformationStrategy
     */
    //public static TransformationFactory createInstance() {
      //return new TransformationFactory();
    //} 
    public GroupNumbersByProximityAlgorithm createGroupNumbersByProximityAlgorithm() {
		Settings s = new Settings();
		String algorithmName = s.getAlgorithmClassName();
        try {
            Class c = Class.forName(PACKAGE_PREFIX + '.' + algorithmName);
            GroupNumbersByProximityAlgorithm algorithm = (GroupNumbersByProximityAlgorithm)c.newInstance();
            return algorithm;
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(GroupNumbersByProximityFactory.class.getName()).log(Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            Logger.getLogger(GroupNumbersByProximityFactory.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            Logger.getLogger(GroupNumbersByProximityFactory.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }
	
}
