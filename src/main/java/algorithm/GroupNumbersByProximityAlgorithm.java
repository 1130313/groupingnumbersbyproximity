package algorithm;

import algorithm.exception.InsufficientNumbersException;

public interface GroupNumbersByProximityAlgorithm {
	public Integer [][] group(Integer [] array, int nrGroups) throws InsufficientNumbersException;
}
