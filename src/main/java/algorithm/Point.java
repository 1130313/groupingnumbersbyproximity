package algorithm;

public class Point {

	private Integer number;
	private Integer groupIndex;
	
	public Point(Integer number, Integer groupIndex)
	{
		this.number = number;
		this.groupIndex = groupIndex;
	}
	public Point(Integer number)
	{
		this.number = number;
		this.groupIndex = 0;
	}
	public Integer getNumber()
	{
		return this.number;
	}
	public Integer getGroupIndex()
	{
		return this.groupIndex;
	}
	public void setNumber(Integer number)
	{
		this.number = number;
	}
	public void setGroupIndex(Integer groupIndex)
	{
		this.groupIndex = groupIndex;
	}
	public static Double distanceBetweenPointAndCenter(Point p1, Double center)
	{
		return Math.abs(p1.getNumber() - center);
	}
	
}
