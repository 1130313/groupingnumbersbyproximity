package main;

import algorithm.UsingDeviationFromMean;
import algorithm.exception.InsufficientNumbersException;
import algorithm.factory.GroupNumbersByProximityFactory;
import settings.Settings;
import algorithm.GroupNumbersByProximityAlgorithm;

public class Main {
	public static void main(String [ ] args)
	{
		//Exemplo
		Integer [] array = {16, 15, 14, 13, 34, 23, 24, 25, 26, 28, 45, 34, 23, 29, 12, 23, 45, 67, 23, 12, 34, 45, 23, 67, 23, 67};
		Integer nrGroups = array.length+1;
		GroupNumbersByProximityFactory fact = new GroupNumbersByProximityFactory();
		GroupNumbersByProximityAlgorithm alg = fact.createGroupNumbersByProximityAlgorithm();
		try {
			alg.group(array, nrGroups);
		} catch (InsufficientNumbersException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
