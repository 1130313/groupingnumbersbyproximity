package test.algorithm;

import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.fail;

import org.junit.Test;

import algorithm.exception.InsufficientNumbersException;
import algorithm.factory.GroupNumbersByProximityFactory;
import algorithm.GroupNumbersByProximityAlgorithm;

public class GroupNumbersByProximityTest {
	
	public void printArray(Integer [] result, int number)
	{		
		System.out.print(" Array " + number + " = ");
		for (int i = 0; i < result.length; i++) {
			System.out.print("["+ result[i]+"]");
		}
		System.out.println();
	}
	
	@Test
	public void testAlgorithmUsingExampleOne()
	{
		System.out.println("Teste 1");
		//input
		Integer [] array = {16, 15, 14, 13, 34, 23, 24, 25, 26, 28, 45, 34, 23, 29, 12, 23, 45, 67, 23, 12, 34, 45, 23, 67, 23, 67};
		Integer nrGroups = 3;
		//perform algorithm
		GroupNumbersByProximityFactory fact = new GroupNumbersByProximityFactory();
		GroupNumbersByProximityAlgorithm algorithm = fact.createGroupNumbersByProximityAlgorithm();
		//expected result
		Integer [] [] expectedResult = {
				{12,12,13,14,15,16},
				{23,23,23,23,23,23,24,25,26,28,29,34,34,34},
				{45,45,45,67,67,67}
		};
		Integer[][] result;
		try {
			result = algorithm.group(array, nrGroups);
			printArray(result[0],1);
			printArray(result[1],2);
			printArray(result[2],3);
			//assert result and expected result
			assertArrayEquals(result[0], expectedResult[0]);
			assertArrayEquals(result[1], expectedResult[1]);
			assertArrayEquals(result[2], expectedResult[2]);
		} catch (InsufficientNumbersException e) {
			e.printStackTrace();
			fail(e.getMessage());
		}


	}
	@Test
	public void testAlgorithmUsingExampleTwo()
	{
		System.out.println("Teste 2");
		
		Integer [] array = {16, 15, 14, 13, 34, 23, 24, 25, 26, 28, 45, 34, 23, 29, 12, 23, 45, 67, 23, 12, 34, 45, 23, 67, 23, 670};
		Integer nrGroups = 3;
		GroupNumbersByProximityFactory fact = new GroupNumbersByProximityFactory();
		GroupNumbersByProximityAlgorithm algorithm = fact.createGroupNumbersByProximityAlgorithm();
		Integer [] [] expectedResult = {
				{12,12,13,14,15,16,23,23,23,23,23,23,24,25,26,28,29},
				{34,34,34,45,45,45,67,67},
				{670}
		};
		Integer[][] result;
		try {
			result = algorithm.group(array, nrGroups);
			printArray(result[0],1);
			printArray(result[1],2);
			printArray(result[2],3);
			assertArrayEquals(result[0], expectedResult[0]);
			assertArrayEquals(result[1], expectedResult[1]);
			assertArrayEquals(result[2], expectedResult[2]);
		} catch (InsufficientNumbersException e) {
			e.printStackTrace();
			fail(e.getMessage());
		}
	}
	
	@Test
	public void testAlgorithmUsingExampleThree()
	{
		System.out.println("Test 3");
		
		Integer [] array = {160, 15, 14, 13, 34, 23, 24, 25, 26, 28, 45, 34, 23, 29, 12, 23, 45, 67, 23, 12, 34, 45, 23, 67, 23, 670};
		Integer nrGroups = 4;
		GroupNumbersByProximityFactory fact = new GroupNumbersByProximityFactory();
		GroupNumbersByProximityAlgorithm algorithm = fact.createGroupNumbersByProximityAlgorithm();
		Integer [] [] expectedResult = {
				{12,12,13,14,15,23,23,23,23,23,23,24,25,26,28,29},
				{34,34,34,45,45,45,67,67},
				{160},
				{670}
		};
		Integer[][] result;
		try {
			result = algorithm.group(array, nrGroups);
			printArray(result[0],1);
			printArray(result[1],2);
			printArray(result[2],3);
			printArray(result[3],4);
			
			assertArrayEquals(result[0], expectedResult[0]);
			assertArrayEquals(result[1], expectedResult[1]);
			assertArrayEquals(result[2], expectedResult[2]);
			assertArrayEquals(result[3], expectedResult[3]);
		} catch (InsufficientNumbersException e) {
			e.printStackTrace();
			fail(e.getMessage());
		}
	}
	@Test
	public void testAlgorithmUsingExampleFour()
	{
		System.out.println("Test 4");
		
		Integer [] array = {16, 15, 14, 13, 34, 23, 24, 25, 26, 28, 45, 34, 23, 29, 12, 23, 45, 67, 23, 12, 34, 45, 23, 67, 23, 67};
		Integer nrGroups = 5;
		GroupNumbersByProximityFactory fact = new GroupNumbersByProximityFactory();
		GroupNumbersByProximityAlgorithm algorithm = fact.createGroupNumbersByProximityAlgorithm();
		Integer [] [] expectedResult = {
				{12,12,13,14,15,16},
				{23,23,23,23,23,23,24,25,26,28},
				{29,34,34,34},
				{45,45,45},
				{67,67,67}
		};
		Integer[][] result;
		try {
			result = algorithm.group(array, nrGroups);
			printArray(result[0],1);
			printArray(result[1],2);
			printArray(result[2],3);
			printArray(result[3],4);
			printArray(result[4],5);
			
			assertArrayEquals(result[0], expectedResult[0]);
			assertArrayEquals(result[1], expectedResult[1]);
			assertArrayEquals(result[2], expectedResult[2]);
			assertArrayEquals(result[3], expectedResult[3]);
			assertArrayEquals(result[4], expectedResult[4]);
		} catch (InsufficientNumbersException e) {
			e.printStackTrace();
			fail(e.getMessage());
		}
	}
	
	
}
